package com.nannapat.lab6;

import javax.management.monitor.Monitor;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank nannapat = new BookBank("Nannapat", 100.0);
        nannapat.deposit(50);
        nannapat.print();
        nannapat.withdraw(50);
        nannapat.print();

        BookBank prayood = new BookBank("Prayood", 1);
        prayood.deposit(100000);
        prayood.print();
        prayood.withdraw(1000000);
        prayood.print();

        BookBank praweet = new BookBank("Praweet", 10);
        praweet.deposit(8888830);
        praweet.print();
        praweet.withdraw(4720);
        praweet.print();
    }
}
