package com.nannapat.lab6;

import static org.junit.Assert.assertEquals;

import javax.swing.plaf.TextUI;

import org.junit.Test;

public class RobotTest {
    @Test
    public void sholdCreateRobotSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(11, robot.getY());
    }

    @Test
    public void sholdCreateRobotSuccess2() {
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }

    @Test
    public void sholdCreateRobotUpSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up();
        assertEquals("Robot", robot.getName());
        assertEquals(10, robot.getY());
    }

    @Test
    public void sholdCreateRobotUpFailAtMin() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MIN);
        boolean result = robot.up();
        assertEquals(false, result);
        assertEquals(Robot.Y_MIN, robot.getY());
    }

    @Test
    public void sholdCreateRobotUpNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(5);
        assertEquals("Robot", robot.getName());
        assertEquals(true, result);
        assertEquals(6, robot.getY());
    }

    @Test
    public void sholdCreateRobotUpNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(11);
        assertEquals("Robot", robot.getName());
        assertEquals(true, result);
        assertEquals(0, robot.getY());
    }


    @Test
    public void sholdCreateRobotUpFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(12);
        assertEquals("Robot", robot.getName());
        assertEquals(false, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void sholdCreateRobotDownSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.down();
        assertEquals(true, result);
        assertEquals(10, robot.getY());
    }

    @Test
    public void sholdCreateRobotDownBeforeMaxSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MAX - 1);
        boolean result = robot.down();
        assertEquals(true, result);
        assertEquals(Robot.Y_MAX, robot.getY());
    }

    @Test
    public void sholdCreateRobotDownAtMax() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MAX);
        boolean result = robot.down();
        assertEquals(false, result);
        assertEquals(Robot.Y_MAX, robot.getY());
    }

    @Test
    public void sholdCreateRobotDownNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.down(5);
        assertEquals("Robot", robot.getName());
        assertEquals(true, result);
        assertEquals(14, robot.getY());
    }

    @Test
    public void sholdCreateRobotDownNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.down(10);
        assertEquals("Robot", robot.getName());
        assertEquals(true, result);
        assertEquals(19, robot.getY());
    }

    
    @Test
    public void sholdCreateRobotDownFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.down(11);
        assertEquals("Robot", robot.getName());
        assertEquals(false, result);
        assertEquals(19, robot.getY());
    }

    @Test
    public void sholdCreateRobotLeftSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.left();
        assertEquals("Robot", robot.getName());
        assertEquals(9, robot.getX());
    }

    @Test
    public void sholdCreateRobotLeftFailAtMin() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MIN, 11);
        boolean result = robot.left();
        assertEquals(false, result);
        assertEquals(Robot.X_MIN, robot.getX());
    }

    @Test
    public void sholdCreateRobotleftBeforeMinSuccess() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MIN + 1, 10);
        boolean result = robot.left();
        assertEquals(true, result);
        assertEquals(Robot.X_MIN, robot.getX());
    }

    @Test
    public void sholdCreateRobotLeftNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.left(5);
        assertEquals("Robot", robot.getName());
        assertEquals(true, result);
        assertEquals(5, robot.getX());
    }

    @Test
    public void sholdCreateRobotLeftNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.left(10);
        assertEquals("Robot", robot.getName());
        assertEquals(true, result);
        assertEquals(0, robot.getX());
    }

    
    @Test
    public void sholdCreateRobotLeftFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.left(11);
        assertEquals("Robot", robot.getName());
        assertEquals(false, result);
        assertEquals(0, robot.getX());
    }

    @Test
    public void sholdCreateRobotRightSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.right();
        assertEquals(true, result);
        assertEquals(11, robot.getX());
    }

    @Test
    public void sholdCreateRobotRightFailAtMax() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MAX, 9);
        boolean result = robot.right();
        assertEquals(false, result);
        assertEquals(Robot.X_MAX, robot.getX());
    }

    @Test
    public void sholdCreateRobotRightAtMax() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MAX, 9);
        boolean result = robot.right();
        assertEquals(false, result);
        assertEquals(Robot.X_MAX, robot.getX());
    }

    @Test
    public void sholdCreateRobotRightBeforeMaxSuccess() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MAX - 1, 10);
        boolean result = robot.right();
        assertEquals(true, result);
        assertEquals(Robot.X_MAX, robot.getX());
    }

    @Test
    public void sholdCreateRobotRightNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.right(5);
        assertEquals("Robot", robot.getName());
        assertEquals(true, result);
        assertEquals(15, robot.getX());
    }

    @Test
    public void sholdCreateRobotRightNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.right(9);
        assertEquals("Robot", robot.getName());
        assertEquals(true, result);
        assertEquals(19, robot.getX());
    }

    
    @Test
    public void sholdCreateRobotRightFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.right(10);
        assertEquals("Robot", robot.getName());
        assertEquals(false, result);
        assertEquals(19, robot.getX());
    }
}
