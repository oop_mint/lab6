package com.nannapat.lab6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    @Test
    public void sholdDepositSuccess() {
        BookBank book = new BookBank("Name", 0);
        boolean result = book.deposit(100);
        assertEquals(true, result);
        assertEquals(100.0, book.getBalance(), 0.00001);
    }

    @Test
    public void sholdDepositNagative() {
        BookBank book = new BookBank("Name", 0);
        boolean result = book.deposit(-100);
        assertEquals(false, result);
        assertEquals(0, book.getBalance(), 0.00001);
    }

    @Test
    public void sholdWithdrawSuccess() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(100) ;
        boolean result = book.withdraw(50);
        assertEquals(true, result);
        assertEquals(50, book.getBalance(), 0.00001);
    }


    @Test
    public void sholdWithdrawNagative() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(100) ;
        boolean result = book.withdraw(-50);
        assertEquals(false, result);
        assertEquals(100, book.getBalance(), 0.00001);
    }


    @Test
    public void sholdWithdrawOverBalance() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(50) ;
        boolean result = book.withdraw(100);
        assertEquals(false, result);
        assertEquals(50, book.getBalance(), 0.00001);
    }


    @Test
    public void sholdWithdrawFit() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(100) ;
        boolean result = book.withdraw(100);
        assertEquals(true, result);
        assertEquals(0, book.getBalance(), 0.00001);
    }
}
